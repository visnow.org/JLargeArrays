/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package org.visnow.jlargearrays;

import org.junit.Assert;
import static org.junit.Assert.*;

/**
 *
 * Assert utilties for floating point numbers and arrays.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class FloatingPointAsserts
{

    private static final double EPSF = 1E-4;
    private static final double EPSD = 1E-13;

    /**
     * Checks if two floating point numbers are equal using the relative error formula.
     * Derived from https://floating-point-gui.de/errors/comparison/
     *
     * @param expected expected value
     * @param actual   actual value
     */
    public static void assertRelativeEquals(float expected, float actual)
    {
        final float absE = Math.abs(expected);
        final float absA = Math.abs(actual);
        final float diff = Math.abs(expected - actual);

        if (expected == actual || (Float.isNaN(expected) && Float.isNaN(actual))) { // shortcut, handles infinities and NaNs
            assertTrue(true);
        } else if (expected == 0 || actual == 0 || (absE + absA < Float.MIN_NORMAL)) {
            // expected or actual is zero or both are extremely close to it
            // relative error is less meaningful here
            assertTrue(diff < (EPSF * Float.MIN_NORMAL));
        } else { // use relative error
            assertTrue(diff / Math.min((absE + absE), Float.MAX_VALUE) < EPSF);
        }
    }

    /**
     * Checks if two floating point numbers are equal using the relative error formula.
     *
     * @param expected expected value
     * @param actual   actual value
     */
    public static void assertRelativeEquals(float expected, double actual)
    {
        assertRelativeEquals(expected, (float) actual);
    }

    /**
     * Checks if two floating point numbers are equal using the relative error formula.
     *
     * @param expected expected value
     * @param actual   actual value
     *
     */
    public static void assertRelativeEquals(double expected, float actual)
    {
        assertRelativeEquals((float) expected, actual);
    }

    /**
     * Checks if two floating point numbers are equal using the relative error formula.
     * Derived from https://floating-point-gui.de/errors/comparison/
     *
     * @param expected expected value
     * @param actual   actual value
     *
     */
    public static void assertRelativeEquals(double expected, double actual)
    {
        final double absE = Math.abs(expected);
        final double absA = Math.abs(actual);
        final double diff = Math.abs(expected - actual);

        if (expected == actual || (Double.isNaN(expected) && Double.isNaN(actual))) { // shortcut, handles infinities nad NaNs
            assertTrue(true);
        } else if (expected == 0 || actual == 0 || (absE + absA < Double.MIN_NORMAL)) {
            // expected or actual is zero or both are extremely close to it
            // relative error is less meaningful here
            assertTrue(diff < (EPSD * Double.MIN_NORMAL));
        } else { // use relative error
            assertTrue(diff / Math.min((absE + absE), Double.MAX_VALUE) < EPSD);
        }
    }

    /**
     * Checks if two arrays of floating point numbers are equal using the relative error formula.
     *
     * @param expected expected value
     * @param actual   actual value
     */
    public static void assertRelativeArrayEquals(float[] expected, float[] actual)
    {
        if (expected == null) {
            Assert.fail("Expected array was null.");
        }
        if (actual == null) {
            Assert.fail("Actual array was null.");
        }

        int actualLength = actual.length;
        int expectedLength = expected.length;
        if (actualLength != expectedLength) {
            Assert.fail("Array lengths differed, expected.length = " + expectedLength + " actual.length = " + actualLength);
        }
        for (int i = 0; i < actualLength; i++) {
            assertRelativeEquals(expected[i], actual[i]);
        }
    }

    /**
     * Checks if two arrays of floating point numbers are equal using the relative error formula.
     *
     * @param expected expected value
     * @param actual   actual value
     *
     */
    public static void assertRelativeArrayEquals(float[] expected, double[] actual)
    {
        if (expected == null) {
            Assert.fail("Expected array was null.");
        }
        if (actual == null) {
            Assert.fail("Actual array was null.");
        }

        int actualLength = actual.length;
        int expectedLength = expected.length;
        if (actualLength != expectedLength) {
            Assert.fail("Array lengths differed, expected.length = " + expectedLength + " actual.length = " + actualLength);
        }
        for (int i = 0; i < actualLength; i++) {
            assertRelativeEquals(expected[i], actual[i]);
        }
    }

    /**
     * Checks if two arrays of floating point numbers are equal using the relative error formula.
     *
     * @param expected expected value
     * @param actual   actual value
     *
     */
    public static void assertRelativeArrayEquals(double[] expected, float[] actual)
    {
        if (expected == null) {
            Assert.fail("Expected array was null.");
        }
        if (actual == null) {
            Assert.fail("Actual array was null.");
        }

        int actualLength = actual.length;
        int expectedLength = expected.length;
        if (actualLength != expectedLength) {
            Assert.fail("Array lengths differed, expected.length = " + expectedLength + " actual.length = " + actualLength);
        }
        for (int i = 0; i < actualLength; i++) {
            assertRelativeEquals(expected[i], actual[i]);
        }
    }

    /**
     * Checks if two arrays of floating point numbers are equal using the relative error formula.
     *
     * @param expected expected value
     * @param actual   actual value
     *
     */
    public static void assertRelativeArrayEquals(double[] expected, double[] actual)
    {
        if (expected == null) {
            Assert.fail("Expected array was null.");
        }
        if (actual == null) {
            Assert.fail("Actual array was null.");
        }

        int actualLength = actual.length;
        int expectedLength = expected.length;
        if (actualLength != expectedLength) {
            Assert.fail("Array lengths differed, expected.length = " + expectedLength + " actual.length = " + actualLength);
        }
        for (int i = 0; i < actualLength; i++) {
            assertRelativeEquals(expected[i], actual[i]);
        }
    }
}
