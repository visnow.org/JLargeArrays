/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class LargeArrayUtilTest extends LargeArrayTest
{

    public LargeArrayUtilTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testLogicLargeArrayArraycopy()
    {

        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LOGIC, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        LogicLargeArray b = new LogicLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(false, b.getBoolean(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getBoolean(srcPos + i), b.getBoolean(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(false, b.getBoolean(i));
        }

        b = new LogicLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getByteData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(false, b.getBoolean(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getBoolean(srcPos + i), b.getBoolean(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(false, b.getBoolean(i));
        }

        byte[] bb = new byte[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new LogicLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(false, b.getBoolean(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getBoolean(srcPos + i), b.getBoolean(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(false, b.getBoolean(i));
        }
    }

    @Test
    public void testLogicLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LOGIC, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i), b.getInt(i));
        }
    }

    @Test
    public void testByteLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.BYTE, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        ByteLargeArray b = new ByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getByte(srcPos + i), b.getByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getByte(i));
        }

        b = new ByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getByteData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getByte(srcPos + i), b.getByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getByte(i));
        }

        byte[] bb = new byte[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ByteLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getByte(srcPos + i), b.getByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getByte(i));
        }
    }

    @Test
    public void testByteLargeArraySubarraycopy()
    {

        byte[] src2 = new byte[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        byte[] dest2 = new byte[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        byte[] expectedDest2 = new byte[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (byte) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        ByteLargeArray src2Large = new ByteLargeArray(src2);
        ByteLargeArray dest2Large = new ByteLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getByteData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getByteData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        byte[] src3 = new byte[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        byte[] dest3 = new byte[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        byte[] expectedDest3 = new byte[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (byte) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        ByteLargeArray src3Large = new ByteLargeArray(src3);
        ByteLargeArray dest3Large = new ByteLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getByteData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getByteData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testByteLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.BYTE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.LOGIC);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i) != 0 ? 1 : 0, b.getByte(i));
        }
    }

    @Test
    public void testUnsignedByteLargeArrayArraycopy()
    {
        UnsignedByteLargeArray a = (UnsignedByteLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        UnsignedByteLargeArray b = new UnsignedByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getUnsignedByte(srcPos + i), b.getUnsignedByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }

        b = new UnsignedByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getUnsignedByteData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getUnsignedByte(srcPos + i), b.getUnsignedByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }

        byte[] bb = new byte[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new UnsignedByteLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getUnsignedByte(srcPos + i), b.getUnsignedByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
    }

    @Test
    public void testUnsignedByteLargeArraySubarraycopy()
    {

        byte[] src2 = new byte[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        byte[] dest2 = new byte[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        byte[] expectedDest2 = new byte[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (byte) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        UnsignedByteLargeArray src2Large = new UnsignedByteLargeArray(src2);
        UnsignedByteLargeArray dest2Large = new UnsignedByteLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getByteData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getByteData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        byte[] src3 = new byte[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        byte[] dest3 = new byte[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        byte[] expectedDest3 = new byte[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (byte) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        UnsignedByteLargeArray src3Large = new UnsignedByteLargeArray(src3);
        UnsignedByteLargeArray dest3Large = new UnsignedByteLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getByteData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getByteData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testUnsignedByteLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i), b.getInt(i));
        }
    }

    @Test
    public void testShortLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        ShortLargeArray b = new ShortLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getShort(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getShort(srcPos + i), b.getShort(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getShort(i));
        }

        b = new ShortLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getShortData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getShort(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getShort(srcPos + i), b.getShort(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getShort(i));
        }

        short[] bb = new short[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ShortLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getShort(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getShort(srcPos + i), b.getShort(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getShort(i));
        }
    }

    @Test
    public void testShortLargeArraySubarraycopy()
    {

        short[] src2 = new short[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        short[] dest2 = new short[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        short[] expectedDest2 = new short[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (short) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        ShortLargeArray src2Large = new ShortLargeArray(src2);
        ShortLargeArray dest2Large = new ShortLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getShortData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getShortData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        short[] src3 = new short[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        short[] dest3 = new short[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        short[] expectedDest3 = new short[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (short) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        ShortLargeArray src3Large = new ShortLargeArray(src3);
        ShortLargeArray dest3Large = new ShortLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getShortData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getShortData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testShortLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i), b.getInt(i));
        }
    }

    @Test
    public void testIntLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.INT, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        IntLargeArray b = new IntLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getInt(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getInt(srcPos + i), b.getInt(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getInt(i));
        }

        b = new IntLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getIntData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getInt(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getInt(srcPos + i), b.getInt(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getInt(i));
        }

        int[] bb = new int[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new IntLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getInt(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getInt(srcPos + i), b.getInt(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getInt(i));
        }
    }

    @Test
    public void testIntLargeArraySubarraycopy()
    {

        int[] src2 = new int[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        int[] dest2 = new int[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        int[] expectedDest2 = new int[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (int) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        IntLargeArray src2Large = new IntLargeArray(src2);
        IntLargeArray dest2Large = new IntLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getIntData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getIntData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        int[] src3 = new int[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        int[] dest3 = new int[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        int[] expectedDest3 = new int[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (int) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        IntLargeArray src3Large = new IntLargeArray(src3);
        IntLargeArray dest3Large = new IntLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getIntData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getIntData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testIntLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.INT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.LONG);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getLong(i), b.getLong(i));
        }
    }

    @Test
    public void testLongLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LONG, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        LongLargeArray b = new LongLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getLong(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getLong(srcPos + i), b.getLong(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getLong(i));
        }

        b = new LongLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getLongData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getLong(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getLong(srcPos + i), b.getLong(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getLong(i));
        }

        long[] bb = new long[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new LongLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getLong(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getLong(srcPos + i), b.getLong(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getLong(i));
        }
    }

    @Test
    public void testLongLargeArraySubarraycopy()
    {

        long[] src2 = new long[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        long[] dest2 = new long[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        long[] expectedDest2 = new long[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (long) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        LongLargeArray src2Large = new LongLargeArray(src2);
        LongLargeArray dest2Large = new LongLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getLongData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getLongData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        long[] src3 = new long[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        long[] dest3 = new long[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        long[] expectedDest3 = new long[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (long) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        LongLargeArray src3Large = new LongLargeArray(src3);
        LongLargeArray dest3Large = new LongLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getLongData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getLongData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testLongLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LONG, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.FLOAT);
        for (int i = 0; i < a.length(); i++) {
            assertRelativeEquals(a.getFloat(i), b.getFloat(i));
        }
    }

    @Test
    public void testFloatlargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        FloatLargeArray b = new FloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeEquals(0, b.getFloat(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeEquals(a.getFloat(srcPos + i), b.getFloat(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeEquals(0, b.getFloat(i));
        }

        b = new FloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getFloatData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeEquals(0, b.getFloat(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeEquals(a.getFloat(srcPos + i), b.getFloat(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeEquals(0, b.getFloat(i));
        }

        float[] bb = new float[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new FloatLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertRelativeEquals(0, b.getFloat(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeEquals(a.getFloat(srcPos + i), b.getFloat(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeEquals(0, b.getFloat(i));
        }
    }

    @Test
    public void testFloatLargeArraySubarraycopy()
    {

        float[] src2 = new float[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        float[] dest2 = new float[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        float[] expectedDest2 = new float[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (float) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2);

        FloatLargeArray src2Large = new FloatLargeArray(src2);
        FloatLargeArray dest2Large = new FloatLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2Large.getFloatData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2Large.getFloatData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2);

        float[] src3 = new float[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        float[] dest3 = new float[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        float[] expectedDest3 = new float[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (float) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3);

        FloatLargeArray src3Large = new FloatLargeArray(src3);
        FloatLargeArray dest3Large = new FloatLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3Large.getFloatData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3Large.getFloatData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testFloatLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.DOUBLE);
        for (int i = 0; i < a.length(); i++) {
            assertRelativeEquals(a.getDouble(i), b.getDouble(i));
        }
    }

    @Test
    public void testDoubleLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        DoubleLargeArray b = new DoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeEquals(0, b.getDouble(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeEquals(a.getDouble(srcPos + i), b.getDouble(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeEquals(0, b.getDouble(i));
        }

        b = new DoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getDoubleData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeEquals(0, b.getDouble(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeEquals(a.getDouble(srcPos + i), b.getDouble(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeEquals(0, b.getDouble(i));
        }

        double[] bb = new double[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new DoubleLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertRelativeEquals(0, b.getDouble(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeEquals(a.getDouble(srcPos + i), b.getDouble(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeEquals(0, b.getDouble(i));
        }
    }

    @Test
    public void testDoubleLargeArraySubarraycopy()
    {

        double[] src2 = new double[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        double[] dest2 = new double[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        double[] expectedDest2 = new double[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = (double) (i + 1);
        }
        expectedDest2[17] = 9;
        expectedDest2[18] = 10;
        expectedDest2[19] = 11;
        expectedDest2[24] = 15;
        expectedDest2[25] = 16;
        expectedDest2[26] = 17;

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2);

        DoubleLargeArray src2Large = new DoubleLargeArray(src2);
        DoubleLargeArray dest2Large = new DoubleLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2Large.getDoubleData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2Large.getDoubleData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2);

        double[] src3 = new double[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        double[] dest3 = new double[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        double[] expectedDest3 = new double[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = (double) (i + 1);
        }

        expectedDest3[109] = 46;
        expectedDest3[110] = 47;
        expectedDest3[116] = 52;
        expectedDest3[117] = 53;
        expectedDest3[123] = 58;
        expectedDest3[124] = 59;
        expectedDest3[151] = 76;
        expectedDest3[152] = 77;
        expectedDest3[158] = 82;
        expectedDest3[159] = 83;
        expectedDest3[165] = 88;
        expectedDest3[166] = 89;

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3);

        DoubleLargeArray src3Large = new DoubleLargeArray(src3);
        DoubleLargeArray dest3Large = new DoubleLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3Large.getDoubleData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3Large.getDoubleData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testDoubleLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.FLOAT);
        for (int i = 0; i < a.length(); i++) {
            assertRelativeEquals(a.getFloat(i), b.getFloat(i));
        }
    }

    @Test
    public void testComplexFloatLargeArrayArraycopy()
    {
        ComplexFloatLargeArray a = (ComplexFloatLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 10);
        int srcPos = 2;
        int destPos = 4;
        int length = (int) a.length() - 2;
        ComplexFloatLargeArray b = new ComplexFloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeArrayEquals(a.getComplexFloat(srcPos + i), b.getComplexFloat(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i));
        }

        b = new ComplexFloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getComplexData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeArrayEquals(a.getComplexFloat(srcPos / 2 + i), b.getComplexFloat(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i));
        }

        float[] bb = new float[4 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ComplexFloatLargeArray(bb);
        for (int i = 0; i < destPos / 2; i++) {
            assertRelativeArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeArrayEquals(a.getComplexFloat(srcPos + i), b.getComplexFloat(destPos / 2 + i));
        }
        for (int i = destPos / 2 + length; i < b.length(); i++) {
            assertRelativeArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i));
        }
    }

    @Test
    public void testComplexFloatLargeArraySubarraycopy()
    {

        float[] src2 = new float[4 * 6 * 2];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        float[] dest2 = new float[5 * 7 * 2];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        float[] expectedDest2 = new float[5 * 7 * 2];

        for (int i = 0; i < 4 * 6; i++) {
            src2[2 * i] = (float) (i + 1);
        }
        expectedDest2[2 * 17] = 9;
        expectedDest2[2 * 18] = 10;
        expectedDest2[2 * 19] = 11;
        expectedDest2[2 * 24] = 15;
        expectedDest2[2 * 25] = 16;
        expectedDest2[2 * 26] = 17;

        ComplexFloatLargeArray src2Large = new ComplexFloatLargeArray(src2);
        ComplexFloatLargeArray dest2Large = new ComplexFloatLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2Large.getComplexData());

        float[] src3 = new float[4 * 5 * 6 * 2];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        float[] dest3 = new float[5 * 6 * 7 * 2];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        float[] expectedDest3 = new float[5 * 6 * 7 * 2];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[2 * i] = (float) (i + 1);
        }

        expectedDest3[2 * 109] = 46;
        expectedDest3[2 * 110] = 47;
        expectedDest3[2 * 116] = 52;
        expectedDest3[2 * 117] = 53;
        expectedDest3[2 * 123] = 58;
        expectedDest3[2 * 124] = 59;
        expectedDest3[2 * 151] = 76;
        expectedDest3[2 * 152] = 77;
        expectedDest3[2 * 158] = 82;
        expectedDest3[2 * 159] = 83;
        expectedDest3[2 * 165] = 88;
        expectedDest3[2 * 166] = 89;

        ComplexFloatLargeArray src3Large = new ComplexFloatLargeArray(src3);
        ComplexFloatLargeArray dest3Large = new ComplexFloatLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3Large.getComplexData());
    }

    @Test
    public void testComplexFloatLargeArrayConvert()
    {
        ComplexFloatLargeArray a = (ComplexFloatLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.DOUBLE);
        for (int i = 0; i < a.length(); i++) {
            assertRelativeEquals(a.getComplexDouble(i)[0], b.getDouble(i));
        }
    }

    @Test
    public void testComplexDoubleLargeArrayArraycopy()
    {
        ComplexDoubleLargeArray a = (ComplexDoubleLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_DOUBLE, 10);
        int srcPos = 2;
        int destPos = 4;
        int length = (int) a.length() - 2;
        ComplexDoubleLargeArray b = new ComplexDoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeArrayEquals(new double[]{0, 0}, b.getComplexDouble(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeArrayEquals(a.getComplexDouble(srcPos + i), b.getComplexDouble(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeArrayEquals(new double[]{0, 0}, b.getComplexDouble(i));
        }

        b = new ComplexDoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getComplexData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertRelativeArrayEquals(new double[]{0, 0}, b.getComplexDouble(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeArrayEquals(a.getComplexDouble(srcPos / 2 + i), b.getComplexDouble(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertRelativeArrayEquals(new double[]{0, 0}, b.getComplexDouble(i));
        }

        double[] bb = new double[4 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ComplexDoubleLargeArray(bb);
        for (int i = 0; i < destPos / 2; i++) {
            assertRelativeArrayEquals(new double[]{0, 0}, b.getComplexDouble(i));
        }
        for (int i = 0; i < length; i++) {
            assertRelativeArrayEquals(a.getComplexDouble(srcPos + i), b.getComplexDouble(destPos / 2 + i));
        }
        for (int i = destPos / 2 + length; i < b.length(); i++) {
            assertRelativeArrayEquals(new double[]{0, 0}, b.getComplexDouble(i));
        }
    }

    @Test
    public void testComplexDoubleLargeArraySubarraycopy()
    {

        double[] src2 = new double[4 * 6 * 2];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        double[] dest2 = new double[5 * 7 * 2];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        double[] expectedDest2 = new double[5 * 7 * 2];

        for (int i = 0; i < 4 * 6; i++) {
            src2[2 * i] = (double) (i + 1);
        }
        expectedDest2[2 * 17] = 9;
        expectedDest2[2 * 18] = 10;
        expectedDest2[2 * 19] = 11;
        expectedDest2[2 * 24] = 15;
        expectedDest2[2 * 25] = 16;
        expectedDest2[2 * 26] = 17;

        ComplexDoubleLargeArray src2Large = new ComplexDoubleLargeArray(src2);
        ComplexDoubleLargeArray dest2Large = new ComplexDoubleLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        assertRelativeArrayEquals(expectedDest2, dest2Large.getComplexData());

        double[] src3 = new double[4 * 5 * 6 * 2];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        double[] dest3 = new double[5 * 6 * 7 * 2];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        double[] expectedDest3 = new double[5 * 6 * 7 * 2];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[2 * i] = (double) (i + 1);
        }

        expectedDest3[2 * 109] = 46;
        expectedDest3[2 * 110] = 47;
        expectedDest3[2 * 116] = 52;
        expectedDest3[2 * 117] = 53;
        expectedDest3[2 * 123] = 58;
        expectedDest3[2 * 124] = 59;
        expectedDest3[2 * 151] = 76;
        expectedDest3[2 * 152] = 77;
        expectedDest3[2 * 158] = 82;
        expectedDest3[2 * 159] = 83;
        expectedDest3[2 * 165] = 88;
        expectedDest3[2 * 166] = 89;

        ComplexDoubleLargeArray src3Large = new ComplexDoubleLargeArray(src3);
        ComplexDoubleLargeArray dest3Large = new ComplexDoubleLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        assertRelativeArrayEquals(expectedDest3, dest3Large.getComplexData());
    }

    @Test
    public void testComplexDoubleLargeArrayConvert()
    {
        ComplexDoubleLargeArray a = (ComplexDoubleLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_DOUBLE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.DOUBLE);
        for (int i = 0; i < a.length(); i++) {
            assertRelativeEquals(a.getComplexDouble(i)[0], b.getDouble(i));
        }
    }

    @Test
    public void testStringLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.STRING, 10);
        String[] data = new String[(int) a.length()];
        for (int i = 0; i < data.length; i++) {
            data[i] = (String) a.get(i);
        }
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        StringLargeArray b = new StringLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        b = new StringLargeArray(2 * length);
        LargeArrayUtils.arraycopy(data, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        String[] bb = new String[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new StringLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }
    }

    @Test
    public void testStringLargeArraySubarraycopy()
    {

        String[] src2 = new String[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        String[] dest2 = new String[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        String[] expectedDest2 = new String[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = Integer.toString(i + 1);
        }
        expectedDest2[17] = Integer.toString(9);
        expectedDest2[18] = Integer.toString(10);
        expectedDest2[19] = Integer.toString(11);
        expectedDest2[24] = Integer.toString(15);
        expectedDest2[25] = Integer.toString(16);
        expectedDest2[26] = Integer.toString(17);

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        StringLargeArray src2Large = new StringLargeArray(src2);
        StringLargeArray dest2Large = new StringLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        String[] src3 = new String[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        String[] dest3 = new String[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        String[] expectedDest3 = new String[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = Integer.toString(i + 1);
        }

        expectedDest3[109] = Integer.toString(46);
        expectedDest3[110] = Integer.toString(47);
        expectedDest3[116] = Integer.toString(52);
        expectedDest3[117] = Integer.toString(53);
        expectedDest3[123] = Integer.toString(58);
        expectedDest3[124] = Integer.toString(59);
        expectedDest3[151] = Integer.toString(76);
        expectedDest3[152] = Integer.toString(77);
        expectedDest3[158] = Integer.toString(82);
        expectedDest3[159] = Integer.toString(83);
        expectedDest3[165] = Integer.toString(88);
        expectedDest3[166] = Integer.toString(89);

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        StringLargeArray src3Large = new StringLargeArray(src3);
        StringLargeArray dest3Large = new StringLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testStringLargeArrayConvert()
    {
        String[] data = new String[]{"a", "ab", "abc", "ąęć", "1234", "test string", "ANOTHER TEST STRING", "", "\n", "\r"};
        StringLargeArray a = new StringLargeArray(data);
        IntLargeArray b = (IntLargeArray) LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i].length(), b.getInt(i));
        }
    }

    @Test
    public void testObjectLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.OBJECT, 10);
        Object[] data = new Object[(int) a.length()];
        for (int i = 0; i < data.length; i++) {
            data[i] = a.get(i);
        }
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        ObjectLargeArray b = new ObjectLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        b = new ObjectLargeArray(2 * length);
        LargeArrayUtils.arraycopy(data, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        Object[] bb = new Object[2 * length];
        for (int i = 0; i < bb.length; i++) {
            bb[i] = new Float(0);
        }
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ObjectLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0f, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0f, b.get(i));
        }
    }

    @Test
    public void testObjectLargeArraySubarraycopy()
    {

        Object[] src2 = new Object[4 * 6];
        long[] srcDim2 = {6, 4};
        long[] srcPos2 = {2, 1};
        Object[] dest2 = new Object[5 * 7];
        long[] destDim2 = {7, 5};
        long[] destPos2 = {3, 2};
        long[] size2 = {3, 2};
        Object[] expectedDest2 = new Object[5 * 7];

        for (int i = 0; i < 4 * 6; i++) {
            src2[i] = Integer.toString(i + 1);
        }
        
        for (int i = 0; i < 5 * 7; i++) {
            dest2[i] = Integer.toString(0);
            expectedDest2[i] = Integer.toString(0);
        }
        
        expectedDest2[17] = Integer.toString(9);
        expectedDest2[18] = Integer.toString(10);
        expectedDest2[19] = Integer.toString(11);
        expectedDest2[24] = Integer.toString(15);
        expectedDest2[25] = Integer.toString(16);
        expectedDest2[26] = Integer.toString(17);

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        ObjectLargeArray src2Large = new ObjectLargeArray(src2);
        ObjectLargeArray dest2Large = new ObjectLargeArray(dest2);

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getData());

        LargeArrayUtils.subarraycopy(src2, srcDim2, srcPos2, dest2Large, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2Large.getData());

        LargeArrayUtils.subarraycopy(src2Large, srcDim2, srcPos2, dest2, destDim2, destPos2, size2);
        Assert.assertArrayEquals(expectedDest2, dest2);

        Object[] src3 = new Object[4 * 5 * 6];
        long[] srcDim3 = {6, 5, 4};
        long[] srcPos3 = {3, 2, 1};
        Object[] dest3 = new Object[5 * 6 * 7];
        long[] destDim3 = {7, 6, 5};
        long[] destPos3 = {4, 3, 2};
        long[] size3 = {2, 3, 2};
        Object[] expectedDest3 = new Object[5 * 6 * 7];

        for (int i = 0; i < 4 * 5 * 6; i++) {
            src3[i] = Integer.toString(i + 1);
        }
        
        for (int i = 0; i < 5 * 6 * 7; i++) {
            dest3[i] = Integer.toString(0);
            expectedDest3[i] = Integer.toString(0);
        }

        expectedDest3[109] = Integer.toString(46);
        expectedDest3[110] = Integer.toString(47);
        expectedDest3[116] = Integer.toString(52);
        expectedDest3[117] = Integer.toString(53);
        expectedDest3[123] = Integer.toString(58);
        expectedDest3[124] = Integer.toString(59);
        expectedDest3[151] = Integer.toString(76);
        expectedDest3[152] = Integer.toString(77);
        expectedDest3[158] = Integer.toString(82);
        expectedDest3[159] = Integer.toString(83);
        expectedDest3[165] = Integer.toString(88);
        expectedDest3[166] = Integer.toString(89);

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);

        ObjectLargeArray src3Large = new ObjectLargeArray(src3);
        ObjectLargeArray dest3Large = new ObjectLargeArray(dest3);

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getData());

        LargeArrayUtils.subarraycopy(src3, srcDim3, srcPos3, dest3Large, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3Large.getData());

        LargeArrayUtils.subarraycopy(src3Large, srcDim3, srcPos3, dest3, destDim3, destPos3, size3);
        Assert.assertArrayEquals(expectedDest3, dest3);
    }

    @Test
    public void testObjectLargeArrayConvert()
    {
        Object[] data = new Object[]{1.12345, -1.54321, 100., -100., Double.MAX_VALUE, Double.MIN_VALUE, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NaN, Double.MIN_NORMAL};
        ObjectLargeArray a = new ObjectLargeArray(data);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.STRING);
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i].toString(), b.get(i));
        }
    }

    @Test
    public void testSelect()
    {
        double[] d = new double[]{1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10};
        byte[] m = new byte[]{0, 0, 1, 0, 1, 1, 1, 0, 0, 0};
        int length = 4;
        LargeArray data = new DoubleLargeArray(d);
        LogicLargeArray mask = new LogicLargeArray(m);
        LargeArray res = LargeArrayUtils.select(data, mask);
        assertEquals(length, res.length());
        assertRelativeEquals(d[2], res.getDouble(0));
        assertRelativeEquals(d[4], res.getDouble(1));
        assertRelativeEquals(d[5], res.getDouble(2));
        assertRelativeEquals(d[6], res.getDouble(3));
    }

    @Test
    public void testGenerateRandom()
    {

        int length = 10;
        LargeArray res = LargeArrayUtils.generateRandom(LargeArrayType.LOGIC, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.LOGIC, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.BYTE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.BYTE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.UNSIGNED_BYTE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.SHORT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.SHORT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.INT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.INT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.LONG, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.LONG, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.FLOAT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.DOUBLE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.COMPLEX_FLOAT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_DOUBLE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.COMPLEX_DOUBLE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.STRING, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.STRING, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.OBJECT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.OBJECT, res.getType());
    }
}
