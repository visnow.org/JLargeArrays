/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class FloatLargeArrayTest extends LargeArrayTest
{

    public FloatLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    
    @Test
    public void testEmptyFloatLargeArray()
    {
        FloatLargeArray a = new FloatLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        
        a = new FloatLargeArray(0, 1, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }
    
    @Test
    public void testFloatLargeArrayEqualsHashCode()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        FloatLargeArray b = new FloatLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setFloat(0, 1f);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testFloatLargeArrayApproximateHashCode()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        FloatLargeArray b = new FloatLargeArray(10);
        a.setFloat(0, 1f);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new FloatLargeArray(10, 0f, true);
        b = new FloatLargeArray(10, 1f, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        FloatLargeArray a = new FloatLargeArray(size);
        FloatLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (FloatLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new FloatLargeArray(size, 2f, true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (FloatLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testFloatLargeArrayConstant()
    {
        FloatLargeArray a = new FloatLargeArray(10, 2.5f, true);
        assertTrue(a.isConstant());
        assertRelativeEquals(2.5f, a.getFloat(0));
        assertRelativeEquals(2.5f, a.getFloat(a.length() - 1));
        a.setFloat(0, 3.5f);
        assertRelativeEquals(3.5f, a.getFloat(0));
        assertFalse(a.isConstant());
       
    }

    @Test
    public void testFloatLargeArrayGetSet()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        long idx = 5;
        float val = 3.4f;
        a.setFloat(idx, val);
        assertRelativeEquals(val, a.getFloat(idx));
        idx = 6;
        a.set(idx, val);
        assertRelativeEquals(val, a.get(idx));
    }

    @Test
    public void testFloatLargeArrayGetSetNative()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            float val = 3.4f;
            a.setToNative(idx, val);
            assertRelativeEquals(val, (float) a.getFromNative(idx));
        }
    }

    @Test
    public void testFloatLargeArrayGetData()
    {
        float[] data = new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f, 8.8f, 9.9f, 10.10f};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        LargeArray.setMaxSizeOf32bitArray(1 << 30);
        FloatLargeArray a = new FloatLargeArray(data);
        float[] res = a.getFloatData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertRelativeEquals(data[(int) i], res[idx++]);
        }
    }
}
