<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.visnow</groupId>
    <artifactId>JLargeArrays</artifactId>
    <name>JLargeArrays</name>
    <version>1.7-SNAPSHOT</version>
    <packaging>jar</packaging>

    <description>Library of one-dimensional arrays that can store up to 2^63 elements.</description>

    <url>https://gitlab.com/visnow.org/JLargeArrays</url> 

    <licenses>
        <license>
            <name>BSD 2-Clause</name>
            <url>http://opensource.org/licenses/BSD-2-Clause</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>p.wendykier</id>
            <name>Piotr Wendykier</name>
            <email>piotr.wendykier@gmail.com</email>
            <organization>VisNow.org</organization>
            <organizationUrl>http://visnow.org</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>+1</timezone>
        </developer>
        <developer>
            <id>b.borucki</id>
            <name>Bartosz Borucki</name>
            <email>b.borucki@icm.edu.pl</email>
            <organization>ICM-UW</organization>
            <organizationUrl>http://www.icm.edu.pl</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>+1</timezone>
        </developer>
        <developer>
            <id>k.nowinski</id>
            <name>Krzysztof Nowiński</name>
            <email>k.nowinski@icm.edu.pl</email>
            <organization>ICM-UW</organization>
            <organizationUrl>http://www.icm.edu.pl</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>+1</timezone>
        </developer>        
    </developers>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <artifactSet>
                                <excludes>
                                    <exclude>junit:junit</exclude>
                                </excludes>
                            </artifactSet>
                            <minimizeJar>false</minimizeJar>
                            <createDependencyReducedPom>false</createDependencyReducedPom>
                            <shadedArtifactAttached>true</shadedArtifactAttached>
                            <shadedClassifierName>with-dependencies</shadedClassifierName>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.3.1</version>
                <configuration>
                    <source>9</source>
                </configuration> 
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>9</source>
                    <target>9</target>
                    <fork>true</fork>
                    <compilerArgs>
                        <arg>--add-modules=jdk.unsupported</arg>
                    </compilerArgs>
                    <debug>true</debug>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <configuration>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <useReleaseProfile>false</useReleaseProfile>
                    <releaseProfiles>release</releaseProfiles>
                    <goals>deploy</goals>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.8</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>ossrh</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <scm>
        <connection>scm:git:git@gitlab.com:visnow.org/JLargeArrays.git</connection>
        <developerConnection>scm:git:git@gitlab.com:visnow.org/JLargeArrays.git</developerConnection>
        <url>git@gitlab.com:visnow.org/JLargeArrays.git</url>
        <tag>HEAD</tag>
    </scm>

    <dependencies>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-math3</artifactId>
            <version>3.6.1</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
        </dependency>
    </dependencies>
    
    <repositories>
        <repository>
            <id>visnoworg-gitlab-maven</id>
            <url>https://gitlab.com/api/v4/groups/7797656/-/packages/maven</url>
        </repository>
    </repositories>
    
    <distributionManagement>   
        <repository>
            <id>visnoworg-gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/375779/packages/maven</url>
        </repository>
        <snapshotRepository>
            <id>visnoworg-gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/375779/packages/maven</url>
        </snapshotRepository>        
    </distributionManagement> 

  
</project>
